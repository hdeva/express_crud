const express = require("express");
const app = express();
const port = 3000;
var mysql = require("mysql");
//  database connection
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "harshi10963",
  database: "crud_express_db",
});

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});
/**
 * executes every sql query,throws err if any error occurs,
 * prints query executed
 * @param {string} sql (SQL query as parameter)
 */
function query(sql) {
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Query Executed");
  });
}
/**
 * executes sql query and views the table,throws err if any error occurs
 * @param {string} tablename
 */
function query_table(tablename) {
  con.query(`select * from ${tablename}`, function (err, result) {
    if (err) throw err;
    console.log(result);
  });
}
/**
 * executes sql query and views the description of table,throws err if any error occurs
 * @param {string} sql
 */
function query_desc(sql) {
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log(result);
  });
}
/**
 * sql query for creating database
 * @param {string} dbname
 */
function createDB(dbname) {
  sql = `create database ${dbname}`;
  query(sql);
  console.log(`Database ${dbname} created`);
}
/**
 * sql query for creating table
 * @param {string} tablename
 */
function createTable(
  tablename,
  col1,
  col1_datatype,
  col2,
  col2_datatype,
  col3,
  col3_datatype,
  col4,
  col4_datatype
) {
  sql = ` CREATE TABLE ${tablename} (${col1} ${col1_datatype},${col2} ${col2_datatype},${col3} ${col3_datatype},${col4} ${col4_datatype})`;
  query(sql);
  console.log(`${tablename} Table Created`);
}
// "create table food (SNo INT PRIMARY KEY,Name VARCHAR(30), Origin varchar(30),Rate int(5))";

/**
 * sql query for inserting values into table
 * @param {string} tablename
 */
function createRecord(tablename, col1, col2, col3, col4, col5) {
  sql = `INSERT INTO ${tablename}  VALUES (${col1},' ${col2}', '${col3}', ${col4},${col5})`;
  query(sql);
  query_table("food");
  console.log("Records Inserted");
  //1,'Panipuri','Uttar Pradesh',20,6),(2,'Idly','Tamilnadu',20,4),(3,'Pav Bhaji','Maharastra',40,1),(4,'Dosa','Karnataka',40,1),(5,'Sushi','Japan',100,1),(6,'Kimchi','Korea',80,1),(7,'Putharekulu','Andhra Pradesh',50,4),(8,'Biryani','Telangana',200,1)";
}
/**
 * sql query for updating table
 * @param {string} tablename
 */
function updateTable(tablename, col5, col5_datatype) {
  sql = `ALTER TABLE ${tablename}  ADD COLUMN ${col5} ${col5_datatype}`;
  query(sql);
  console.log(`Table updated , column ${col5} added`);
}
/**
 * sql query for updating records
 * @param {string} tablename
 */
function updateRecord(tablename, col4, col5) {
  sql = `update ${tablename} set ${col4}=50 where ${col5}>4`;
  query(sql);
  console.log(`record updated`);
  query_table("food");
}
/**
 * sql query for viewing records
 * @param {string} tablename
 */

function readRecord(tablename) {
  sql = `select * from  ${tablename}`;
  query(sql);
  query_table("food");
  console.log("Read Records");
}
/**
 * sql query for viewing description of database
 * @param {string} dbname
 */
function readDB(dbname) {
  sql = `use ${dbname}`;
  query(sql);
  query_desc(sql);
  console.log("Read Database");
}
/**
 * sql query for viewing description of table
 * @param {string} tablename
 */
function readTable(tablename) {
  sql = `desc ${tablename}`;
  query(sql);
  query_desc(sql);
  console.log("Read Table");
}
/**
 * sql query for deleting records from table
 * @param {string} tablename
 */
function deleteRecord(tablename) {
  sql = `delete from ${tablename}`;
  query(sql);
  console.log("All Records deleted");
}
/**
 * sql query for deleting table
 * @param {string} tablename
 */
function deleteTable(tablename) {
  sql = `drop table ${tablename}`;
  query(sql);
  console.log("Table deleted");
}
/**
 * sql query for deleting database
 * @param {string} dbname
 */
function deleteDB(dbname) {
  sql = `drop database ${dbname}`;
  query(sql);
  console.log("database deleted");
}
/**
 * sql query for renaming database
 * @param {string} tablename
 */
// function updateDB(old_dbname, new_dbname) {
//    only for and before version MySQL 5.1.7

//   sql = `rename database ${old_dbname} to ${new_dbname}`;

//   query(sql);
//   console.log("Database updated");
// }

/**
 * sends response creating database in path localhost:3000/1 using post method
 * req= request object, res= response object
 */
app.post("/1", (req, res) => {
  //create database
  res.send(createDB("crud_express_db"));
});

/**
 * sends response creating table in path localhost:3000/2 using post method
 * req= request object, res= response object
 */
app.post("/2", (req, res) => {
  res.send(
    // create table
    createTable(
      "food",
      "SNo",
      "INT PRIMARY KEY",
      "Name",
      "VARCHAR(30)",
      "Origin",
      "varchar(30)",
      "Rate",
      "int(5)"
    )
  );
});
/**
 * sends response updating table in path localhost:3000/3 using putt method
 * req= request object, res= response object
 */
app.put("/3", (req, res) => {
  //update table
  res.send(updateTable("food", "Quantity", " int(5)"));
});
/**
 * sends response creating records in path localhost:3000/4 using post method
 * req= request object, res= response object
 */
app.post("/4", (req, res) => {
  res.send(
    createRecord("food", 1, "Panipuri", "Uttar Pradesh", 20, 6)
    // createRecord("food", 2, "Idly", "Tamilnadu", 20, 4)
    // createRecord("food", 3, "Pav Bhaji", "Maharastra", 40, 1)
    // createRecord("food", 4, "Dosa", "Karnataka", 40, 1)
    // createRecord("food", 5, "Sushi", "Japan", 100, 1)
    // createRecord("food", 6, "Kimchi", "Korea", 80, 1)
    //  createRecord("food", 7, "Putharekulu", "Andhra Pradesh", 50, 4)
    //createRecord("food", 8, "Biryani", "Telangana", 200, 1)
  );
});

/**
 * sends response updating records in path localhost:3000/5 using put method
 * req= request object, res= response object
 */
app.put("/5", (req, res) => {
  res.send(
    // update record

    updateRecord("food", "Rate", "Quantity")
  );
});
/**
 * sends response reading records in path localhost:3000/6 using get method
 * req= request object, res= response object
 */
app.get("/6", (req, res) => {
  res.send(
    // read record

    readRecord("food")
  );
});

/**
 * sends response reading description of table in path localhost:3000/7 using get method
 * req= request object, res= response object
 */
app.get("/7", (req, res) => {
  res.send(
    // read db

    readDB("crud_express_db")
  );
});

/**
 * sends response reading description of database in path localhost:3000/8 using get method
 * req= request object, res= response object
 */
app.get("/8", (req, res) => {
  //read table
  res.send(readTable("food"));
});


/**
 * sends response deleting records of table in path localhost:3000/9 using delete method
 * req= request object, res= response object
 */
app.delete("/9", (req, res) => {
  res.send(
    // delete record

    deleteRecord("food")
  );
});

/**
 * sends response dropping table in path localhost:3000/10 using delete method
 * req= request object, res= response object
 */
app.delete("/10", (req, res) => {
  res.send(
    // delete table

    deleteTable("food")
  );
});

/**
 * sends response deleting database in path localhost:3000/11 using delete method
 * req= request object, res= response object
 */
app.delete("/11", (req, res) => {
  res.send(
    // delete database

    deleteDB("crud_express_db")
  );
});
/**
 * sends response updating database in path localhost:3000/12 using put method
 * req= request object, res= response object
 */
//   app.put("/12", (req, res) => {
//     res.send(// update table

//     updateDB("harshidb", "cruddb"));
//   });


//Listening to port
app.listen(port, () => {
  console.log(`Express app listening on port ${port}`);
});
